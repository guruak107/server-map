// flow-typed signature: 155d4ad80b8460c421be3a3d868aa53f
// flow-typed version: <<STUB>>/configstore_v3.1.2/flow_v0.73.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'configstore'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'configstore' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'configstore/index' {
  declare module.exports: $Exports<'configstore'>;
}
declare module 'configstore/index.js' {
  declare module.exports: $Exports<'configstore'>;
}
